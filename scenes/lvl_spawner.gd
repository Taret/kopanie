extends Node2D

var lvl_altitude = {}
export (int) var easy
export (int) var medium 
export (int) var hard
var lvls_path = 'res://scenes/levels'

var easy_path  = 'res://scenes/levels'
var medium_path = 'res://scenes/levels/2/'
var hard_path = 'res://scenes/levels/3/'


var all_lvls = []
var lvls_easy = []
var lvls_medium = []
var lvls_hard = [] 

var altitude
var spawn_position = 10200
var can_pick = false
var spawned_lvl_counter = 1 
var spawn_altitude = 40 #in the middle

func _ready():
	
	lvl_altitude['easy'] = easy 
	lvl_altitude['medium'] = medium
	lvl_altitude['hard'] = hard 
	
	Global.lvl_altitude = lvl_altitude
	
	
	sort_lvls(easy_path)
	sort_lvls(medium_path)
	sort_lvls(hard_path)
	
func _process(delta):

	altitude = Global.altitude
	draw_lvl(altitude)
	control_spawning_time(altitude, spawned_lvl_counter)
	
	
func control_spawning_time(alt, count):
	"""Controls can_pick flag via initial_spawn_altitude. If ship is after middle of the current level
	it allows for spawning next level"""
	#print(alt, count * spawn_altitude)
	
	if alt > spawn_altitude:
		print('spawn altitude: ', spawn_altitude)
		spawn_altitude += 50
		can_pick = true
		

func sort_lvls(path):
	all_lvls = list_files_in_directory(path)
	
	for lvl in all_lvls:
		if 'easy' in lvl:
			lvls_easy.append(lvl)
			
		elif 'medium' in lvl:
			lvls_medium.append(lvl)
			
		elif 'hard' in lvl:
			lvls_hard.append(lvl)
			

func list_files_in_directory(path):
	
    var files = []
    var dir = Directory.new()
    dir.open(path)
    dir.list_dir_begin()

    while true:
        var file = dir.get_next()
        if file == "":
            break
        elif not file.begins_with("."):
            files.append(file)

    dir.list_dir_end()
    return files
	
	
func draw_lvl(alt):
	""" Randomly picks a lvl according to lvl_altitude data """
	
	if can_pick:
	
		#easy picking
		if alt < lvl_altitude['easy']:
			var lvl = pick(lvls_easy)
			var loaded_lvl = load_lvl(lvl, easy_path)
			place_lvl(loaded_lvl, Vector2(0, spawn_position))
			print('alt_spawn: ', altitude, ', ', loaded_lvl.get_name())
			#print(spawn_position + 10200 + 816)
			loaded_lvl.del_alt = spawn_position + 10200 + 816
			increase_spawn_position()
			

		#medium picking
		elif alt > lvl_altitude['easy'] and alt < lvl_altitude['medium']:
			var lvl = pick(lvls_medium)
			var loaded_lvl = load_lvl(lvl, medium_path)
			print('alt_spawn: ', altitude, ', ', loaded_lvl.get_name())
			place_lvl(loaded_lvl, Vector2(0, spawn_position))
			loaded_lvl.del_alt = spawn_position + 10200 + 816
			increase_spawn_position()
			
		
		#hard picking
		else:
			#print(lvls_hard)
			var lvl = pick(lvls_hard)
			var loaded_lvl = load_lvl(lvl, hard_path)
			print('alt_spawn: ', altitude, ', ', loaded_lvl.get_name())
			place_lvl(loaded_lvl, Vector2(0, spawn_position))
			print('Placed lvl: ', loaded_lvl.get_name())
			loaded_lvl.del_alt = spawn_position + 10200 + 816
			increase_spawn_position()
		
		

func pick(list):
	""" Randomly picks an element from the list"""
	var lvl = list[randi() % list.size()]
	return lvl 
	
func load_lvl(lvl, folder_path):
	"""Loads level from levels folder based on str(lvl) suffix. Returns instanced scene object"""
	
	var path = folder_path + '/' + lvl 
	var loaded_lvl = load(path).instance()
	can_pick = false 
	return loaded_lvl
	
func place_lvl(loaded_lvl, pos):
	""" Spawns loaded level on map at given pos"""
	
	var lvl_instance = loaded_lvl
	lvl_instance.position = pos
	add_child(lvl_instance)
	spawned_lvl_counter += 1 
	
	
	
func increase_spawn_position():
	""" Tracks where next level should be spawned
	10200 - corresponds to 50 tiles"""
	spawn_position += 10200
	return spawn_position
	
	
	
#	pos_node.global_position(pos.x, pos.y)
#	pos_node.add_child(loaded_lvl)
	
	
	
	

