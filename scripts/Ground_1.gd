extends Area2D

var drilled = false
var background


func _ready():
	pass

func _process(delta):
	drill_ground()


func drill_ground():
	if drilled and handle_raycast():
		destroy()
		
func handle_raycast():
	return $RayCast2D.is_colliding()
	
	
func destroy():
	
	background = preload('res://scenes/Background_1.tscn').instance()
		
	background.position = position 
	get_parent().add_child(background)
	
	
	#get_tree().get_root().add_child(background)
	queue_free()
		

