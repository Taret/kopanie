extends Area2D

var drilled = false
var up_value = 2200

func _ready():
	Global.Energy = self
	up_value = 2200

func _process(delta):
	drill_ground()

func destroy():
	
	var background = preload('res://scenes/Background_1.tscn').instance()
	background.position = position 
	get_parent().add_child(background)
	
	got_energy(up_value)
	Global.Sounds.play_energy_up()
	queue_free()

func drill_ground():
	if drilled and handle_raycast():
		destroy()
		
func handle_raycast():
	return $RayCast2D.is_colliding()
	
			
		
func got_energy(up_value):
	
	Global.En_bar.add(up_value)

