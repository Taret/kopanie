extends Node2D

onready var _ring = $qte_ring_red
onready var _min = 0.51
onready var _max = 0.66
export (float) var factor
export (int) var penalty1
export (int) var penalty2


func _ready():
	$qte_Start.play()
	pass

func _process(delta):
	scale_ring(factor, delta)
	
func scale_ring(factor, delta):
	
	if _ring.scale.x > 0:
		_ring.scale.x -= factor * delta
		_ring.scale.y -= factor * delta

func _on_TouchScreenButton_pressed():
	
	if _ring.scale.x < _max and _ring.scale.x > _min:
		
		Global.Sounds.play_qte_correct()
		Global.En_bar.subtract(penalty2)
		get_parent().get_parent().destroy()
		
		
	else:
		Global.Sounds.play_qte_fail()
		Global.En_bar.subtract(penalty1)
		get_parent().get_parent().destroy()
		
	




	