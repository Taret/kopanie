extends Control

onready var _highscore = $Highscore
onready var _score = $Score

func _ready():

	_score.text = str(Global.score)
	_highscore.text = str(Global.read_savegame())
	

func _process(delta):
	read_input()

func read_input():
	if Input.is_action_just_pressed("quit"):
		get_tree().quit()
		
	if Input.is_action_just_pressed("reload"):
		get_tree().change_scene("res://scenes/Main.tscn")


func _on_Play_Again_pressed():
	get_tree().change_scene("res://scenes/Main.tscn")


func _on_Main_Menu_pressed():
	get_tree().change_scene("res://scenes/Start_scene.tscn")
