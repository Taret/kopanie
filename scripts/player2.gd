extends KinematicBody2D

var player_speed = Vector2(0,0)

func _ready():
	Global.Player = self

func _physics_process(delta):
	print(global_position)
	
	var collision = move_and_collide(player_speed * delta)
	
	if collision: #check if collided with anything
		var collider = collision.get_collider() #read collider

		if collider.is_in_group("ground"):
			collider.drilled = true 		
