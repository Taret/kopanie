extends Node2D
export (bool) var increase_speed 
export (int) var max_speed
export (int) var increment_speed
export (PackedScene) var laser
var near_nd_r
var near_nd_l
onready var _drill_animation = $Area2D/Drill
var can_shoot = false #altitude controled by under_thresh
var player_speed_temp
var score


var drill_stopped = false



var player_speed = Vector2(0,0)

func _ready():
	Global.Player = self
	drill_start()
	
func drill_start():
	_drill_animation.play('drill_start')
	drill_stopped = false

	

func _on_Drill_animation_finished():
	
	if _drill_animation.animation == 'drill_start':
		_drill_animation.play('drill_normal')
		
	elif _drill_animation.animation == 'drill_stop':
		drill_stopped = true
		
	elif _drill_animation.animation == 'drill_broken':
		get_tree().change_scene("res://scenes/End_scene.tscn")

		
func drill_stop():
	
	if is_digging() == false:
		_drill_animation.play('drill_stop')
		
	elif drill_stopped:
		drill_start()
		
		
func drill_broken():
	_drill_animation.play('drill_broken')

func _process(delta):
	
	move_down(delta)
	increase_speed(delta)
	under_thresh(1)
	drill_stop()
	
	
func under_thresh(alt):
	if Global.alt_Meter.altitude > alt:
		can_shoot = true

func spawn_laser():
		
	
	if can_shoot and (Global.Laser_bar.value<800 or Global.Laser_bar.value == 0) :
		
		Global.Sounds.play_laser_shoot()
		var laser_inst = laser.instance()
		$Area2D.add_child(laser_inst)
		player_speed.y = player_speed_temp
		

func increase_speed(delta):
	
	if increase_speed and player_speed.y < max_speed and is_digging():
		player_speed.y += increment_speed * delta
		player_speed_temp = player_speed.y
		Global.GameState.player_speed.y = player_speed.y

func is_digging():
	if player_speed.y > 0:
		
		return true
		
	else:
		return false
	


func move_down(delta):
	global_position.y += player_speed.y * delta

	
func turn_r():
	
	var action = handle_raycast_r()
	
	match action:
		'no_qte':
			if Global.GameState.can_turn and not(near_nd_r):
				global_position.x += 204
				
			
		'qte':
			pass
			
		'non_destructible':
			pass
			

func turn_l():
	
	var action = handle_raycast_l()
	
	match action:
		'no_qte':
			if Global.GameState.can_turn and not(near_nd_l):
				global_position.x -= 204
				
				
			
		'qte':
			pass
			
		'non_destructible':
			pass
	


func near_undestructible_r():
	if $Area2D/RayCast2D_r2.is_colliding():
		var collider = $Area2D/RayCast2D_r.get_collider()
		
		if collider.is_in_group('non_destructible'):
			return true


func handle_raycast_r():
	""" checks if player is near qte object or normal ground"""
	if $Area2D/RayCast2D_r.is_colliding():
		
		var collider = $Area2D/RayCast2D_r.get_collider()
		
		if collider.is_in_group('ground') or collider.is_in_group('energy') :
			return 'no_qte'
			
		elif collider.is_in_group('qte'):
			return 'qte'
			
		elif collider.is_in_group('non_destructible'):
			return 'non_destructible'
			
	else:
		return 'no_qte'

func handle_raycast_l():
	""" checks if player is near qte object or normal ground"""
	if $Area2D/RayCast2D_l.is_colliding():
		
		var collider = $Area2D/RayCast2D_l.get_collider()
		
		if collider.is_in_group('ground'):
			return 'no_qte'
			
		elif collider.is_in_group('qte'):
			return 'qte'
			
		elif collider.is_in_group('non_destructible'):
			return 'non_destructible'
			
	else:
		return 'no_qte'
			

func _on_Area2D_area_entered(area):
	
	if area.is_in_group('ground'):
		area.drilled = true 
		player_speed = Global.GameState.player_speed
		
	elif area.is_in_group('non_destructible'):
		get_parent().get_node("Sounds/impact_Rock").play()
		player_speed.y = 0 
		var hit_dmg = area.hit_dmg
		got_dmg(hit_dmg)
		
	if area.is_in_group('energy'):
		#get_parent().get_node("Sounds/energy_Picked").play()
		area.drilled = true 
		player_speed = Global.GameState.player_speed
		
	if area.is_in_group('mineral'):
		
		area.drilled = true 
		player_speed = Global.GameState.player_speed
		get_parent().get_node("Sounds/mineral_Picked").play()
		
		
	elif area.is_in_group('qte'):
		player_speed.y = 0 
		area.drilled = true
		
		
	
func got_dmg(hit_dmg):
	Global.En_bar.subtract(hit_dmg)


func _on_right_nd_check_area_entered(area):
	if area.is_in_group('non_destructible'):
		near_nd_r = true
		

func _on_right_nd_check_area_exited(area):
	if area.is_in_group('non_destructible'):
		
		near_nd_r = false


func _on_left_nd_check2_area_entered(area):
	if area.is_in_group('non_destructible'):
		near_nd_l = true
		


func _on_left_nd_check2_area_exited(area):
	if area.is_in_group('non_destructible'):
		
		near_nd_l = false


func _on_VisibilityNotifier2D_screen_exited():
	
	end_game()

func end_game():
	Global.inGame = false
	score = Global.score
	
	
	if score > Global.read_savegame():
			Global.save(score)
			
	get_tree().change_scene("res://scenes/End_scene.tscn")
			
