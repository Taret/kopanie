extends Node2D

onready var _label = $Label
var altitude =  0
var tile_dim = 204
var start_position = 0 
var record = false 

var score = 0 
var best_score 


func _ready():
	Global.alt_Meter = self
	best_score = Global.read_savegame()
	var start_position = Global.Player.global_position.y
	


func _process(delta):
	
	score = altitude + 10*Global.minerals
	Global.score = score
	increaseAltitude()
	check_record()
	pulse()



func increaseAltitude():
	if Global.inGame and Global.Player.is_digging():

		if Global.Player.global_position.y > tile_dim * altitude:
			altitude += 1
			_label.text = str(round(altitude))
			Global.altitude = altitude
			
		
func check_record():
	
	if score > best_score:
		record = true 
		
		
func pulse():
	if record:
		$Particles2D.emitting=true
	
		
#func calculate_time_interval():
#	var player_speed = Global.GameState.Player.player_speed.y
#	var time = player_speed / 204
		

