extends TextureProgress

export (int) var maximum_value
export (int) var minimum_value
export (float) var increment
export (int) var shoot_val
var score


func _ready():
	Global.Laser_bar = self
	max_value = maximum_value
	min_value = minimum_value
	

func _process(delta):
	
	increase_bar()


func increase_bar():
	if value < maximum_value:
		value -= increment

func decrease():
	value += shoot_val
	

	
