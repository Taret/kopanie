extends Node2D

var generative_objects = [preload('res://scenes/Ground_1.tscn'),
						preload('res://scenes/Ground_2.tscn'),
						preload('res://scenes/Rocks_1.tscn')
						]

func _ready():
	for item in range(get_child_count()):
		var ground_gen = get_child(item)
		ground_gen.add_child(draw_tile())
		


#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass


func draw_tile():
	randomize()
	var tile_idx = generative_objects[randi() % generative_objects.size()]
	var tile_obj = tile_idx.instance()
	return tile_obj