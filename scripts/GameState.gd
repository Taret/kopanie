extends Node2D
export (int) var PLAYER_SPEED 
export (int) var DMG 
var player_speed
var Alt_bar
var hit_dmg
var can_turn = true
export (int) var up_value

func _ready():
	
	Global.GameState = self 
	
	player_speed = Vector2(0, PLAYER_SPEED)
	Global.Player.player_speed.y = PLAYER_SPEED
	
	hit_dmg = DMG
	Global.inGame = true
	
	

func _process(delta):
	read_input()

func read_input():
	if Input.is_action_just_pressed("quit"):
		Global.Player.drill_broken()
		
	if Input.is_action_just_pressed("reload"):
		get_tree().reload_current_scene()


