extends TextureProgress

export (int) var maximum_value
export (int) var minimum_value
export (float) var increment
var score


func _ready():
	max_value = maximum_value
	min_value = minimum_value
	Global.En_bar = self 
	

func _process(delta):
	
	
	var player_speed = Global.Player.player_speed
	
	if player_speed.y > 0:
		
		decrease_bar()
		
	if value > maximum_value - 500:
		end_game()
	

		
func end_game():	

	Global.inGame = false
	score = Global.alt_Meter.score
	
	if score > Global.read_savegame():
		Global.save(score)
	
	Global.score = score
	Global.Player.drill_broken()
		
func decrease_bar():

	
	
		
	value += increment
	
func subtract(hit_dmg):
	value += hit_dmg
	
	
func add(add_val):
	value -= add_val
