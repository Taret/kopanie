extends Node2D

func _ready():
	Global.Sounds = self 

func _on_drill_Normal_ground_finished():
	$drill_Normal_ground.play()


func _on_OST_finished():
	$OST.play()
	

func play_qte_correct():
	$qte_Correct.play()
	
	
func play_qte_fail():
	$qte_Fail.play()
	
	
func play_energy_up():
	$energy_Picked.play()
	
	
func play_laser_shoot():
	$laser_Shot.play()
