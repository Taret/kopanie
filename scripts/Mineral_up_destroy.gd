extends Node2D

var spawned = false 

func _ready():
	spawned = true 

	
	pass
	
func _process(delta):
	decrease_vis(delta)
	

func decrease_vis(delta):
	var alpha = $Sprite.modulate.a
	
	if alpha > 0:
		$Sprite.modulate.a -= 0.5 * delta
	else:
		queue_free()
	
#	if $Sprite.modulate.a > 0 :
#		$Sprite.modulate.a -= 0.01
