extends Area2D
var spawn_qte = false
var drilled = false
var qte_instance = preload("res://scenes/qte_object.tscn").instance()
var not_drilled = true

func _ready():
	pass

func _process(delta):
	drill_ground()
	qte()
	
func destroy():
	
	var background = preload('res://scenes/Background_1.tscn').instance()
	background.position = position 
	get_parent().add_child(background)
	
	Global.Player.player_speed = Global.GameState.player_speed
	Global.GameState.can_turn = true
	queue_free()
		
func drill_ground():
	if not_drilled:
		if drilled and handle_raycast():
			spawn_qte = true
			not_drilled = false
		
func handle_raycast():
	return $RayCast2D.is_colliding()
	
func qte():
	if spawn_qte:
	
		$qte_position.add_child(qte_instance)
		spawn_qte = false
		Global.Player.player_speed.y = 0
		Global.GameState.can_turn = false
		
		

		
		


