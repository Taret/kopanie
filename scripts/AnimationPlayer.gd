extends AnimationPlayer

func _ready():
	pass


func _on_SwipeDetector_swiped(direction):
	if direction == Vector2(1,0):
		print("swiped right")
		play("SwipeRight")
	elif direction == Vector2(-1,0):
		print("swiped left")
		play("SwipeLeft")
