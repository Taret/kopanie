extends CanvasLayer

var bestscore

func _ready():
	bestscore = Global.read_savegame()
	$Score_loaded.text = str(bestscore)

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass


func _on_Back_pressed():
	get_tree().change_scene("res://scenes/Start_scene.tscn")
