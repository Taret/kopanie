extends Area2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	Global.Laser_bar.decrease()
	$Timer.start()
	pass


func _on_Area2D_area_entered(area):
	area.destroy()


func _on_Timer_timeout():
	queue_free()
