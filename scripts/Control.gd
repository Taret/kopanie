extends Control

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass


func _on_TouchScreenButton_pressed():
	get_tree().change_scene("res://scenes/Main.tscn")


func _on_Credits_pressed():
	get_tree().change_scene("res://scenes/Credits1.tscn")


func _on_Bestscore_pressed():
	get_tree().change_scene("res://scenes/Bestscore.tscn")


func _on_Tutorial_pressed():
	get_tree().change_scene("res://scenes/Tutorial1.tscn")


func _on_Exit_pressed():
	get_tree().quit()
